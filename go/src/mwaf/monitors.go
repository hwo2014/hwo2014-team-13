package main

import (
	"fmt"
)

func findCarPosition(car CarId, positions []CarPosition) CarPosition {
	for _, carPos := range positions {
		if carPos.Id.Name == car.Name && carPos.Id.Color == car.Color {
			return carPos
		}
	}
	return CarPosition{}
}

func nextPieceIsBend(g *Game, carPos CarPosition) bool {
	current := carPos.PiecePosition.PieceIndex
	nextIndex := (current + 1) % len(g.race.Track.Pieces)
	nextPiece := g.race.Track.Pieces[nextIndex]
	return nextPiece.Radius != 0
}

func breakMonitor(g *Game) {
	pos := g.pos.Listen()
	for {
		positions := <-pos
		carPos := findCarPosition(g.ownCar, positions.([]CarPosition))
		if nextPieceIsBend(g, carPos) {
			sendThrottle(g, 0.35)
		} else {
			sendThrottle(g, 0.6)
		}
	}
}

func laneMonitor(g *Game) {
	pos := g.pos.Listen()
	targetLane := 1
	sentLane := 0
	for {
		select {
		case positions := <-pos:
			carPos := findCarPosition(g.ownCar, positions.([]CarPosition))
			currentLane := carPos.PiecePosition.Lane.EndLaneIndex
			fmt.Printf("Current: %d, sent: %d, target: %d\n", currentLane, sentLane, targetLane)
			if currentLane != targetLane &&
				sentLane != targetLane &&
				currentLane == sentLane {

				if sentLane < targetLane {
					sentLane++
					sendSwitch(g, Right)
				} else if sentLane > targetLane {
					sentLane--
					sendSwitch(g, Left)
				}
			}
		}
	}
}

func turboMonitor(g *Game) {

}
