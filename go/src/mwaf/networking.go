package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
)

func connect(host string, port int, g *Game) net.Conn {
	fmt.Printf("Connecting to %s:%d\n", host, port)
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		logAndExit(err)
	}
	r := bufio.NewReader(conn)
	w := bufio.NewWriter(conn)

	go readMessages(r, g.in)
	go sendMessages(w, g.out)
	return conn
}

func sendMessages(w *bufio.Writer, out chan MsgOut) {
	for {
		err := sendMsg(w, <-out)
		if err != nil {
			logAndExit(err)
		}
	}
}

func readMessages(r *bufio.Reader, in chan MsgIn) {
	for {
		input, err := readMsg(r)
		if err != nil {
			logAndExit(err)
		}
		in <- input
	}
}

func sendMsg(w *bufio.Writer, v interface{}) (err error) {
	payload, err := json.Marshal(v)
	fmt.Printf("Sending: %s\n", string(payload))
	if err != nil {
		log.Printf("Unable to marshal JSON from %+v due to %v\n", v, err)
		return
	}
	_, err = w.Write(payload)
	if err != nil {
		log.Printf("Unable to write message for %+v due to %v\n", v, err)
		return
	}
	_, err = w.WriteString("\n")
	if err != nil {
		log.Printf("Unable to write newline for %+v due to %v\n", v, err)
		return
	}
	w.Flush()
	return nil
}

func readMsg(reader *bufio.Reader) (msg MsgIn, err error) {
	line, err := reader.ReadString('\n')
	fmt.Printf("Input: %s", line)
	if err != nil {
		log.Printf("Unable to read message due to %v", err)
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		log.Printf("Unable to unmarshal message %s du eto %v", line, err)
		return
	}
	return
}
