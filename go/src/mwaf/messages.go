package main

import (
	"encoding/json"
)

const (
	// messages sent
	CIJoin     = "join"
	JoinRace   = "joinRace"
	CreateRace = "createRace"
	Throttle   = "throttle"
	Turbo      = "turbo"
	SwitchLane = "switchLane"
	Right      = "Right"
	Left       = "Left"
	Ping       = "ping"

	// message received
	YourCar        = "yourCar"
	GameInit       = "gameInit"
	GameStart      = "gameStart"
	CarPositions   = "carPositions"
	TurboAvailable = "turboAvailable"
	Crash          = "crash"
	Spawn          = "spawn"
	LapFinished    = "lapFinished"
	Finish         = "finish"
	DNF            = "dnf"
	GameEnd        = "gameEnd"
	TournamentEnd  = "tournamentEnd"
)

type MsgIn struct {
	Type     string          `json:"msgType"`
	Data     json.RawMessage `json:"data"`
	GameId   string          `json:"gameId,omitempty"`
	GameTick int             `json:"gameTick,omitempty"`
}

type MsgOut struct {
	Type     string      `json:"msgType"`
	Data     interface{} `json:"data"`
	GameId   string      `json:"gameId,omitempty"`
	GameTick int         `json:"gameTick,omitempty"`
}

type InitData struct {
	BotId     BotId  `json:"botId"`
	TrackName string `json:"trackName"`
	Password  string `json:"password,omitempty"`
	CarCount  int    `json:"carCount"`
}

type BotId struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

type GameInfo struct {
	Race Race `json:"race"`
}

type Race struct {
	Track       Track       `json:"track"`
	Cars        []Car       `json:"cars"`
	RaceSession RaceSession `json:"raceSession"`
}

type Track struct {
	Id            string        `json:"id"`
	Name          string        `json:"name"`
	Pieces        []Piece       `json:"pieces"`
	Lanes         []LaneSpec    `json:"lanes"`
	StartingPoint StartingPoint `json:"startingPoint"`
}

type Piece struct {
	Length float32 `json:"length"`
	Switch bool    `json:"switch"`
	Radius float32 `json:"radius"`
	Angle  float32 `json:"andle"`
}

type LaneSpec struct {
	DistanceForCenter float32 `json:"distanceFromCenter"`
	Index             int     `json:"index"`
}

type StartingPoint struct {
	Position Coord   `json:"position"`
	Angle    float32 `json:"angle"`
}

type Coord struct {
	X float32 `json:"x"`
	Y float32 `json:"y"`
}

type Car struct {
	Id         CarId         `json:"id"`
	Dimensions CarDimensions `json:"dimensions"`
}

type CarId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type CarDimensions struct {
	Length       float32 `json:"length"`
	Width        float32 `json:"width"`
	FlagPosition float32 `json:"guideFlagPosition"`
}

type RaceSession struct {
	Laps       int  `json:"laps"`
	MaxLapTime int  `json:"maxLapTimeMs"`
	QuickRace  bool `json:"quickRace"`
}

type CarPosition struct {
	Id            CarId         `json:"id"`
	Angle         float32       `json:"angle"`
	PiecePosition PiecePosition `json:"piecePosition"`
}

type PiecePosition struct {
	PieceIndex      int     `json:"pieceIndex"`
	InPieceDistance float32 `json:"inPieceDistance"`
	Lane            Lane    `json:"lane"`
	Lap             int     `json:"lap"`
}

type Lane struct {
	StartLaneIndex int `json:"startLaneIndex"`
	EndLaneIndex   int `json:"endLaneIndex"`
}
