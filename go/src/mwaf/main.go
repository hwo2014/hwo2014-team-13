package main

import (
	"flag"
	"log"
	"os"
)

type GameParams struct {
	host     string
	port     int
	name     string
	key      string
	track    string
	password string
	cars     int
	init     string
}

func main() {
	params := parseArgs()
	game := createGame()
	conn := connect(params.host, params.port, game)
	defer conn.Close()
	runGame(game, params)
}

func parseArgs() GameParams {
	host := flag.String("host", "testserver.helloworldopen.com", "Server address.")
	port := flag.Int("port", 8091, "Server port.")
	name := flag.String("name", "mwaf", "Bot name.")
	// yeah yeah, putting the bot key in source is stupid, but it's in the repo anyway
	key := flag.String("key", "zqx1PbNBxF7xcw", "Bot key.")
	track := flag.String("track", "keimola", "Track to race.")
	password := flag.String("password", "", "Password for race.")
	cars := flag.Int("cars", 1, "Amount of cars to race.")
	init := flag.String("init", "ci", "Game init type, ci, join or create.")
	flag.Parse()
	return GameParams{*host, *port, *name, *key, *track, *password, *cars, *init}
}

func logAndExit(err error) {
	log.Fatal(err)
	os.Exit(1)
}

func exit() {
	// maybe do some cleanup?
	os.Exit(0)
}
