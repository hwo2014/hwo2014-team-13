package main

import (
	"encoding/json"
	"fmt"
)

type Broadcast struct {
	listeners []chan interface{}
}

func (b *Broadcast) Send(v interface{}) {
	for _, c := range b.listeners {
		c <- v
	}
}
func (b *Broadcast) Listen() (c chan interface{}) {
	c = make(chan interface{})
	b.listeners = append(b.listeners, c)
	return
}

type Game struct {
	gameId string
	ownCar CarId
	race   Race
	in     chan MsgIn
	out    chan MsgOut
	pos    Broadcast
}

func createGame() (g *Game) {
	g = &Game{
		gameId: "",
		ownCar: CarId{},
		race:   Race{},
		in:     make(chan MsgIn),
		out:    make(chan MsgOut),
		pos:    Broadcast{[]chan interface{}{}},
	}
	go breakMonitor(g)
	go laneMonitor(g)
	go turboMonitor(g)
	return
}

func runGame(g *Game, params GameParams) {
	sendInit(g, params)
	for {
		// why not just send g? becasuse I want to be explicit
		// that's we're waiting on input in this for loop
		dispatch(g, <-g.in)
	}
}

func dispatch(g *Game, msg MsgIn) {
	switch msg.Type {
	case CIJoin:
		fmt.Println("CI/quickrace  joined.")
	case JoinRace:
		fmt.Println("Game joined.")
	case CreateRace:
		fmt.Println("Game created.")
	case YourCar:
		json.Unmarshal(msg.Data, &g.ownCar)
		g.gameId = msg.GameId
		fmt.Printf("GameId: %s\n", g.gameId)
		fmt.Printf("Car: %s (%s)\n", g.ownCar.Name, g.ownCar.Color)
	case GameInit:
		gameInfo := GameInfo{}
		err := json.Unmarshal(msg.Data, &gameInfo)
		if err != nil {
			logAndExit(err)
		}
		g.race = gameInfo.Race
		fmt.Println("Game initialized.")
	case GameStart:
		fmt.Println("Game starting!")
	case Crash:
		// TODO if own car :P
		fmt.Println("Crashed!")
		// TODO some learning
	case Spawn:
		// TODO if own car :P
		fmt.Println("Respawning.")
		sendThrottle(g, 1)
	case CarPositions:
		if msg.GameTick == 0 {
			sendThrottle(g, 1)
			return
		}
		var positions []CarPosition
		json.Unmarshal(msg.Data, &positions) // ignore err for now
		g.pos.Send(positions)
	case TurboAvailable:
		fmt.Println("Has trubo.")
		// send to turbochan!
	case TournamentEnd:
		exit()
	default:
		fmt.Printf("Unsupported type %s, ignoring.\n", msg.Type)
	}
}

func sendInit(g *Game, params GameParams) {
	var msgType string

	// for whatever reason the CI needs it own special "join"
	// message rather than accepting the joinRace or creatRace
	// commands
	switch params.init {
	default:
		fallthrough
	case "ci":
		g.out <- MsgOut{
			Type: CIJoin,
			Data: BotId{
				Name: params.name,
				Key:  params.key,
			},
		}
		return
	case "join":
		msgType = JoinRace
	case "create":
		msgType = CreateRace
	}

	// send for join and create
	g.out <- MsgOut{
		Type: msgType,
		Data: InitData{
			BotId: BotId{
				Name: params.name,
				Key:  params.key,
			},
			TrackName: params.track,
			Password:  params.password,
			CarCount:  params.cars,
		},
	}
}

func sendPing(g *Game) {
	g.out <- MsgOut{
		Type: Ping,
	}
}

func sendThrottle(g *Game, throttle float32) {
	g.out <- MsgOut{
		Type: Throttle,
		Data: throttle,
	}
}

func sendSwitch(g *Game, direction string) {
	g.out <- MsgOut{
		Type: SwitchLane,
		Data: direction,
	}
}
